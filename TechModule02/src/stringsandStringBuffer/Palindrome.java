package stringsandStringBuffer;
import java.io.*;
import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		Scanner sb=new Scanner(System.in);
		System.out.println("Enter your String:");
		String name=sb.nextLine();
		StringBuffer s=new StringBuffer(name);
		String str=s.reverse().toString();
		if(name.equals(str)) {
			System.out.println("Palindrome");
		}
		else {
			System.out.println("Not a Palindrome");
		}
		
		

	}

}
