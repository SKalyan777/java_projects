package com.techademy.Mindtree;
import java.util.Scanner;

public class DoubleArrayAddition {

	public static void main(String[] args) {
		Scanner a =new Scanner(System.in);
		System.out.println("Enter first array length:");
		int n1=a.nextInt();
		double[] arr1=new double[n1];
		System.out.println("Enter the elements:");
		for(int i=0;i<n1;i++) {
			arr1[i]=a.nextDouble();
		}
		System.out.println("Enter second array length:");
		int n2=a.nextInt();
		double[] arr2=new double[n2];
		System.out.println("Enter the elements:");
		for(int i=0;i<n2;i++) {
			arr2[i]=a.nextDouble();
		}
		int maxlen=0,minlen=0;
		if(arr1.length>arr2.length) {
			maxlen=arr1.length;
			minlen=arr2.length;
		}
		else {
			maxlen=arr2.length;
			minlen=arr1.length;
		}
		
		int[] arr3=new int[maxlen];
		for(int i=0;i<minlen;i++) {
			arr3[i]=(int)(arr1[i]+arr2[i]);	
		}
		for(int j=minlen;j<maxlen;j++) {
			if(arr1.length>arr2.length) {
				arr3[j]=(int)(arr1[j]);
				System.out.println(arr3[j]);
			}
			else {
				arr3[j]=(int)(arr2[j]);
				System.out.println(arr3[j]);;
			}
		}
		for(int x:arr3) {
			System.out.print(x+" ");
		}
	
		
		
	}
}