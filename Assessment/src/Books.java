

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
@Entity
@Table(name="Book")
public class Books {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	public int id;
	@Column(name="Book Name")
	private String BookName;
	@Column(name="Author Name")
	private String AuthorName;
	@Column(name="Description")
	private String Description;
	@Column(name="Genre")
	private String genre;
	@Column(name="Price")
	private float price;
	
	
	public Books() {
		super();
	}
	public Books(int idnumber,String bookName, String authorName, String description, String genre, float price) {
		super();
		id=idnumber;
		BookName = bookName;
		AuthorName = authorName;
		Description = description;
		this.genre = genre;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return BookName;
	}
	public void setBookName(String bookName) {
		BookName = bookName;
	}
	public String getAuthorName() {
		return AuthorName;
	}
	public void setAuthorName(String authorName) {
		AuthorName = authorName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Book [id=" + id + ", BookName=" + BookName + ", AuthorName=" + AuthorName + ", Description="
				+ Description + ", genre=" + genre + ", price=" + price + "]";
	}
		

}
