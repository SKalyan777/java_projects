package stringsandStringBuffer;

import java.util.Scanner;

public class CombinationOf2Strings {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.print("Enter your Strings : ");
		String str=s.nextLine();
		String str2=s.nextLine();
		int k=-1,m=-1;
		for(int i=1;i<=(str.length()+str2.length());i++) {
			if(i%2!=0 && (i/2)<=str.length()) {
				k=k+1;
				System.out.print(str.charAt(k));
			}
			else if(i%2==0 && (i/2)<=str2.length()){
				m=m+1;
				System.out.print(str2.charAt(m));
			}
			else {
				break;
			}
		}

	}

}
