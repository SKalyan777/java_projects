package www.wipro.tech1;
import java.util.*;
import java.util.Scanner;
class Fruits{
	private int fruitId;
	private String fruitName;
	private int Price;
	private int rating;
	Fruits(int id,String name,int price,int rating){
		this.fruitId=id;
		this.fruitName=name;
		this.Price=price;
		this.rating=rating;
	}
	public int getFruitId() {
		return fruitId;
	}
	public void setFruitId(int fruitId) {
		this.fruitId = fruitId;
	}
	public String getFruitName() {
		return fruitName;
	}
	public void setFruitName(String fruitName) {
		this.fruitName = fruitName;
	}
	public int getPrice() {
		return Price;
	}
	public void setPrice(int price) {
		Price = price;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
}
public class MyClass {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		Fruits[] f=new Fruits[4];
		for(int i=0;i<4;i++) {
			int id=in.nextInt();in.nextLine();
			String name=in.nextLine();
			int p=in.nextInt();in.nextLine();
			int rate=in.nextInt();
			f[i]=new Fruits(id,name,p,rate);	
		}
		in.nextLine();
		int met=in.nextInt();
		int r=findMaximumPriceByRating(f,met);
		if(r==0) {
			System.out.println("No such Fruit");
		}
		else {
			System.out.println(r);
		}
		
	}
	public static int findMaximumPriceByRating(Fruits[] f,int rating) {
		int z=0;
		List<Integer> arr=new ArrayList<Integer>();
		for(int i=0;i<f.length;i++){
			if(f[i].getRating()>rating) {
				z=f[i].getFruitId();
				arr.add(z);
			
			}
		}
		z=Collections.max(arr);
		return z;
	}

}
