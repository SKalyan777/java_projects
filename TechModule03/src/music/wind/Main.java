package music.wind;

import java.util.Scanner;
public class Main
{
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		in.nextLine();
		int length=String.valueOf(n).length();
		int even=0;
		int odd=0;
		while(n!=0){
		    n=n%10;
		    if(length%2==0){
		        even=even+n;
		        length--;
		    }
		    else{
		        odd=odd+n;
		        length--;
		    }
		}
		if(odd==even){
		    System.out.println("Sum are equal");
		}
		else{
		    System.out.println("Sum are not equal");
		}
	}
}
