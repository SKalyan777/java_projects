package polymorphism;
class Shape {

	 void draw() {
		System.out.println("Drawing Shape");
	}
	void erase(){
		System.out.println("Erasing shape");
	}

}
class Circle extends Shape{
	@Override
	void draw() {
		System.out.println("Drawing Circle");
	}
	
	void erase(){
		System.out.println("Erasing Circle");
	}
	
}
class Triangle extends Shape{
	
	void draw() {
		System.out.println("Drawing Triangle");
	}
	void erase(){
		System.out.println("Erasing Triangle");
	}
	
}
class Square extends Shape{
	void draw() {
		System.out.println("Drawing Square");
	}
	void erase(){
		System.out.println("Erasing Square");
	}
}
public class Overriding{
	public static void main(String[] args) {
		Shape s=new Circle();
		Shape b=new Triangle();
		Shape c=new Square();
		System.out.println("Overriding the draw and erase methods of Shape Class ");
	s.draw();
	s.erase();
	b.draw();
	b.erase();
	c.draw();
	c.erase();
	}
	
}
