package service;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.util.Scanner;

import entity.BookStore;


public class DemoClass {

	public static void main(String[] args) {
		String response;
		operations meth = new operations();
		int id;
		String BName;
		String AName;
		String Desc;
		String Gen;
		float p;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your name:");
		String name=sc.nextLine();
		System.out.println("Welcome "+name+" !");
		do {
		System.out.println("Please enter your choice:");
		System.out.println("1.To add a book");
		System.out.println("2.To update a book");
		System.out.println("3.To delete a book");
		System.out.println("4.To display all books");
		System.out.println("5.To display count of books");
		System.out.println("6.To display books with autobiography genre");
		int choice =sc.nextInt();
		switch(choice) {
		case 1:
		   System.out.println("Enter the book name");
		   BName=sc.next();
		   System.out.println("Enter the author name");
		   AName = sc.next();
		   System.out.println("Enter the description");
		   Desc = sc.next();
		   System.out.println("Enter the Genre");
		   Gen = sc.next();
		   System.out.println("Enter the price");
		   p = sc.nextFloat();
		   BookStore newBook = new BookStore(BName,AName,Desc,Gen,p);
		   meth.AddBook(newBook);
		   break;
	case 2:System.out.println("Enter the book id that you want to delete");
	   	   id = sc.nextInt();
	   	   System.out.println("Enter the choice of the entity that u want to change:");
		   System.out.println("1.BookName 2.AuthorName 3.Price ");
		   int ch=sc.nextInt();
		   switch(ch) {
		   case 1:meth.updateBook(id);
		          break;
		   case 2:meth.updateBookAuthor(id);
		   		  break;
		   case 3:meth.updateBookPrice(id);
		   	      break;
		   }
		   break;
		  
	case 3:System.out.println("Enter the book id that you want to delete");
		   id = sc.nextInt();
		   meth.deleteBook(id);
		   break;
	case 4:meth.display();
		   break;
	case 5:meth.BooksCount();
	       break;
	case 6:meth.AutoBiographyGenre();
	       break;
		   
	}
		System.out.println("Enter y for continue and n for exit");
		 response = sc.next();
		}while(response.equalsIgnoreCase("y"));
	    sc.close();
	}

}
