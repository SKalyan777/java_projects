package com.techademy.Mindtree;
import java.util.Scanner;

public class MatrixAddition {

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		System.out.println("Enter the size of first matrix:");
		int m=a.nextInt();
		int n=a.nextInt();
		int[][] arr1=new int[m][n];
		System.out.println("Enter the size of second matrix");
		int p=a.nextInt();
		int q=a.nextInt();
		int[][] arr2=new int[p][q];
		int[][] c=new int[m][n];
		if(m!=p || n!=q) {
			System.out.println("Matrix addition not possible");
		}
		else {
			System.out.println("Enter first matrix elements:");
		for(int i=0;i<m;i++) {
			for(int j=0;j<n;j++) {
				arr1[i][j]=a.nextInt();
			}
		}
		System.out.println("Enter second matrix elements:");
			for(int i=0;i<m;i++) {
				for(int j=0;j<n;j++) {
					arr2[i][j]=a.nextInt();
				}
			}
		}
		System.out.println("Resultant matrix");
		for(int i=0;i<m;i++) {
			for(int j=0;j<n;j++) {
				c[i][j]=arr1[i][j]+arr2[i][j];
				System.out.print(c[i][j]+" ");
			}
			System.out.println();
		}
	}

}
