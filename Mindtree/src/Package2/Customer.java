package Package2;

public class Customer {
	private long id;
	private String name;
	private char gender;
	private String email;
	private String contactNumber;
	public Customer(long id, String name, char gender, String email, String contactNumber) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.contactNumber = contactNumber;
	}
	public long getIs() {
		return id;
	}
	public void setIs(long is) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Customer id : "+id+"\n"+"Customer: "+name+"\n"+"Customer contact details :"+contactNumber+","+email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	

}
