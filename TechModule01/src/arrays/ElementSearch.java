/* searching an element in a given array*/
package arrays;
import java.util.*;
public class ElementSearch {

	public static void main(String[] args) {
		int[] arr= {1,4,5,6,7,8};
		int c=-1;
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter your number: ");
		int n=sc.nextInt();
		for(int i=0;i<arr.length;i++) {
			if(arr[i]==n) {
				c=i;
				break;
			}
		}
		System.out.println(c);

	}

}
