/* Reverse of a number*/
package controlStstements;
import java.util.*;
public class Reverse {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the number: ");
		int a=input.nextInt();
		int m,r=0;
		while(a!=0) {
			m=a%10;
			r=r*10+m;
			a=a/10;
		}
		System.out.println(r);
	}

}
