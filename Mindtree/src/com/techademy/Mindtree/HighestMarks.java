package com.techademy.Mindtree;

public class HighestMarks {
	private int id;
	private String name;
	private String branch;
	private double score;
	public HighestMarks(int id,String name,String branch,double score) {
		super();
		this.id=id;
		this.branch=branch;
		this.name=name;
		this.score=score;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getBranch() {
		return branch;
	}
	public double getScore() {
		return score;
	}

}
