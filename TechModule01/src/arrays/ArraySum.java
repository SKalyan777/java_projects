/* Sum and Average of 1-D array*/
package arrays;
public class ArraySum {

	public static void main(String[] args) {
		int[] arr= {23,45,67,89,4};
		int s=0;
		for(int i=0;i<arr.length;i++) {
			s=s+arr[i];
		}
		System.out.println("Sum is "+s);
		System.out.println("Average is "+(float)s/arr.length);

	}

}
