package inheritance;

import java.util.Scanner;

class Video{
	String videoName;
	boolean checkout;
	int rating;
	Video(String name){
		this.videoName=name;
	}
	public String getName() {
		return videoName;
	}
	
	void doCheckout() {
		checkout=true;
	}
	void doReturn() {
		checkout=false;
	}
	boolean getCheckout() {
		return checkout;
	}
	
	void receiveRating(int r) {
		this.rating=r;
	}
	int getRating() {
		return rating;
	}
	
}
class VideoStore{
	Video[] store;
	int size;
	void addVideo(String name) {
		Video newvideo=new Video(name);
		try {
			size=store.length;}
		catch(Exception e) {
			size=0;
			
		}
			Video[] newstore=new Video[size+1];
			newstore[size]=newvideo;
			store=newstore;
		
		}
	
	int checkVideo(String name) {
		int k=0;
		for(Video newvideo:store) {
			if(newvideo.getName().equals(name)) {
				k=1;
			}
		}
		return k;
	}
	void doCheckout(String name) {
		if(store.length==0 || store==null) {
			System.out.println("Store is empty");
		}
		for(Video newvideo:store ) {
			if(newvideo.getName().equals(name)) {
				newvideo.doCheckout();
			}
		}
	}
	
	void doReturn(String name) {
		if(store.length==0 || store==null) {
			System.out.println("Store is empty");
		}
		for(Video newvideo:store) {
			if(newvideo.getName().equals(name)) {
				newvideo.doReturn();
			}
		}
		}
	void receiveRating(String name,int rate) {
		if(store.length==0|| store==null) {
			System.out.println("Store is empty or video not found");
		}
		else {
			for(Video newvideo:store) {
				if(newvideo.getName().equals(name)) {
					newvideo.receiveRating(rate);
				}
			}
		}
	}
	void listInventory() {
		try {
			size=store.length;}
		catch(Exception e) {
			size=0;
			
		}
		if(size==0) {
			System.out.println("Store is empty or video not found");}
		else {
			for(Video newvideo: store) {
				System.out.println("------------------------------------------");
				System.out.println("Video Name"+"  "+"Checkout Status"+"  "+" Rating ");
				System.out.println(newvideo.getName()+"\t\t"+newvideo.getCheckout()+"\t\t"+newvideo.getRating());
				System.out.println("------------------------------------------");
			}
		}
	}
	
	
}

public class VideoLauncher {

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		VideoStore vstore=new VideoStore();
		int i=0;
		while(i!=6) {
		System.out.println("1.Add Videos : ");
		System.out.println("2.Check out Video : ");
		System.out.println("3.Return Video : ");
		System.out.println("4.Receive Rating : ");
		System.out.println("5.List Inventory : ");
		System.out.println("6.Exit : ");
		System.out.print("Enter your choice 1.....6 : ");
		String name;
		i=a.nextInt();
		switch(i){
		case 1:
			System.out.print("Enter the videoname:");
			name=a.next();
			vstore.addVideo(name);
			System.out.println("Video "+name+" Successfully");
			break;
		case 2:
			System.out.print("Enter the videoname that you want to checkout:");
			name=a.next();
			if(vstore.checkVideo(name)==0) {
				System.out.println("Video doesn't exists");
			}
			else {
				vstore.doCheckout(name);
				System.out.println("video "+name+" checked out successfully");
			}
			break;
		case 3:
			System.out.print("Enter the videoname that you want to return:");
			name=a.next();
			if(vstore.checkVideo(name)==0) {
				System.out.println("Video doesn't exists");
			}
			else {
				vstore.doReturn(name);
				System.out.print("video "+name+" returned  successfully");
			}
			break;
		case 4:
			System.out.print("Enter the videoname that you want give rating :");
			name=a.next();
			if(vstore.checkVideo(name)==0) {
				System.out.println("Video doesn't exists");
			}
			else {
				System.out.print("Enter the Rating: ");
				int r=a.nextInt();
				vstore.receiveRating(name, r);
				System.out.println("Rating "+r+" "+"has been updated to your video");
			}
			break;
		case 5:
			vstore.listInventory();
			break;
		default:
			System.out.println("Exiting ..............");
			break;
			
		}
		
	}
	}

}
