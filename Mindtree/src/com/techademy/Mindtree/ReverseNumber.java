package com.techademy.Mindtree;
import java.util.Scanner;
public class ReverseNumber {

	public static void main(String[] args) {
		Scanner a =new Scanner(System.in);
		System.out.print("Enter your number : ");
		int n=a.nextInt();
		String s = "";
		int k=0;
		while(n!=0) {
			k=n%10;
			s=s+k;
			n=n/10;
		}
		System.out.println("Reversed number is : "+s);
	}

}
