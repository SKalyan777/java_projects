package com.techademy.Mindtree;

public class ConsoleName {

	public static void main(String[] args) {
		
		//Taking the input as an argument from the console
		
		System.out.println("Hello, "+args[0]+"!");

	}

}
