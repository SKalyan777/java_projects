package com.techademy.Mindtree;

public class BookStore {
	private String name;
	private String title;
	private Double price;
	private int year;
	public BookStore(String name, String title, Double price,int year) {
		super();
		this.name = name;
		this.title = title;
		this.price = price;
		this.year=year;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	

}
