/*Printing even numbers in the given range*/
package controlStstements;
import java.util.Scanner;
public class EvenNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		System.out.println("Enter two numbers : ");
		int a=input.nextInt();
		int b=input.nextInt();
		for(int i=a;i<=b;i++) {
			if(i%2==0) {
				System.out.println(i);
			}
		}

	}

}
