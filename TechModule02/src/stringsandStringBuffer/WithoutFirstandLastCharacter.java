package stringsandStringBuffer;

import java.util.Scanner;

public class WithoutFirstandLastCharacter {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.print("Enter your String : ");
		String str=s.nextLine();
		int n=str.length();
		String c=str.substring(1,n-1);
		System.out.println(c);

	}

}
