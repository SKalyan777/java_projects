package Package4;
import java.util.Scanner;

public class CarModel {

	private String licenseNumber;
	private String model;
	private double currentMileage;
	private int engineSize;
	public CarModel(String licenseNumber, String model, double currentMileage, int engineSize) {
		super();
		this.licenseNumber = licenseNumber;
		this.model = model;
		this.currentMileage = currentMileage;
		this.engineSize = engineSize;
	}
	
	public String getLicenseNumber() {
		return licenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public double getCurrentMileage() {
		return currentMileage;
	}
	public void setCurrentMileage(double currentMileage) {
		this.currentMileage = currentMileage;
	}
	public int getEngineSize() {
		return engineSize;
	}
	public void setEngineSize(int engineSize) {
		this.engineSize = engineSize;
	}
	
		public static void findCarModelList(String model,CarModel[] c) {
			int count=0;
			for(int i=0;i<10;i++) {
				if(model.equals(c[i].getModel())) {
					System.out.println(c[i].getLicenseNumber()+"| "+c[i].getModel()+"|"+c[i].getCurrentMileage()+"| "+c[i].getEngineSize());
							count++;
				}
			}
			if(count==0) System.out.println("No CarModels found");
		}



public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	CarModel[] c=new CarModel[10];
	for(int i=0;i<10;i++) {
		System.out.println("Enter car"+i+"name: ");
		String name = sc.nextLine();
		System.out.println("Enter car Model: ");
		String model = sc.nextLine();
		System.out.println("Enter the mileage");
		double mileage = sc.nextDouble();
		System.out.println("Enter the engine size: ");
		int enginesize = sc.nextInt();sc.nextLine();
		c[i]= new CarModel(name,model,mileage,enginesize);	
	}
	System.out.println("Enter car model to search");
	String m=sc.nextLine();
	findCarModelList(m,c);
}
}
