package com.techademy.Mindtree;

import java.util.Scanner;

public class HailstoneSequence {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		int c=0;
		while(true) {
		if(n%2==0) {
		
			System.out.println(n+" is even so i take half : "+(n/2));
			n=n/2;
			c+=1;
		}
		else {
			
			System.out.println(n+" is odd so i make 3n+1 : "+(3*n+1));
			n=3*n+1;
			c+=1;
		}
		if(n==1) {
			break;
		}
	  }	
		System.out.println("There are total "+c+" steps to reach 1");
	}
}
