package com.techademy.Mindtree;
import java.util.Scanner;

public class Multiplication {

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		System.out.println("Enter the size of  first matrix rows X Columns");
		int r=a.nextInt();
		int c=a.nextInt();
		int[][] arr=new int[r][c];
		System.out.println("Enter the elements of matrix");
		for(int i=0;i<r;i++) {
			for(int j=0;j<c;j++) {
				arr[i][j]=a.nextInt();
			}
		}
		System.out.println("Enter the size of  second matrix rows X Columns");
		int r2=a.nextInt();
		int c2=a.nextInt();
		int[][] arr2=new int[r2][c2];
		System.out.println("Enter the elements of matrix");
		for(int i=0;i<r2;i++) {
			for(int j=0;j<c2;j++) {
				arr2[i][j]=a.nextInt();
			}
		}
		if(r==c2 && c==r2) {
			int[][] arr3 = new int[r][c2];
	        for(int i=0;i<r;i++) {
	            for (int j=0;j<c2;j++) {
	                for (int k=0;k<c;k++) {
	                    arr3[i][j]+= arr[i][k]*arr2[k][j];
	                }
	            }
	        }
	        System.out.println("Resultant Matrix");
	        for(int i=0;i<r;i++) {
	        	for(int j=0;j<c2;j++) {
	        		System.out.print(arr3[i][j]+" ");
	        	}
	        	System.out.println();
	        }
			
		}
		else {
			System.out.println("Matrix multiplication not possibel");
		}

	}

}
