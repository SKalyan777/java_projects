package abstraction;
import java.util.Random;
import java.util.Scanner;
abstract class Compartment{
	
	public abstract String notice();
}
class Ladies extends Compartment{
	public String notice() {
		return("Ladies compartment");
	}
}
class FirstClass extends Compartment{
	public String notice() {
		return("Firstclass compartment");
	}
}
class General extends Compartment{
		public String notice() {
			return("General compartment");
		}
}
class Luggage extends Compartment{
			public String notice() {
				return("Luggage compartment");
			}
}
public class TestCompartment {
	public static void main(String[] args) {
		Compartment[] compartments=new Compartment[10];
		Random r=new Random();
		for(int i=0;i<10;i++) {
			/*Generating random numbers using random module
			Math.random() * (max - min)) + min)'''*/
			int num=r.nextInt(4-1)+1;
			if(num==1) {
				compartments[i]=new Ladies();
			}
			else if(num==2){
				compartments[i]=new FirstClass();
			}
			else if(num==3){
				compartments[i]=new General();}
			else if (num==4){
				compartments[i]=new Luggage();}
			System.out.println(compartments[i].notice());
		}
		
	}

}
