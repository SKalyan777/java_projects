import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class DemoTest {

	public static void main(String[] args) throws Exception {
		
		//if dependencies not added we get classNotFoundException
		//Class.forName("com.mysql.jdbc.Driver"); for MySQL-5.x
		Class.forName("com.mysql.cj.jdbc.Driver"); //for MySQL-8.0.x
		System.out.println("Driver loaded successfully");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/batch8","root","Rgukt@7601073764");
		System.out.println("Connected successfully");

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the id:");
		int id =sc.nextInt();
		System.out.println("enter the name");
		String name=sc.next();
		System.out.println("enter the salary");
		float salary=sc.nextFloat();
		PreparedStatement pstmt = conn.prepareStatement("insert into employee values(?,?,?)");
		pstmt.setInt(1, id);
		pstmt.setString(2, name);
		pstmt.setFloat(3, salary);
		int res = pstmt.executeUpdate();           //DML operation 
		if(res>0) {
			System.out.println("Record inserted sucessfully");
		}
		 
	}

}
