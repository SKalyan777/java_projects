package com.techademy.Mindtree;
import java.util.Scanner;

public class ShapeArea {
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the shape number 1.Square 2.Rectangle 3.Circle 4.Triangle");
		int choice=sc.nextInt();
		switch(choice) {
		case 1: System.out.print("Enter side of a square : ");
				int side=sc.nextInt();
				double c=side*side;
				System.out.println("Area of square is : "+String.format("%.2f", c));
				break;
				
		case 2:
				System.out.print("Enter length and breadth of rectangle : ");
				int length=sc.nextInt();
				int breadth=sc.nextInt();
				double r=length*breadth;
				System.out.println("Area of rectangle is : "+String.format("%.2f", r));
				break;
		case 3:
				System.out.print("Enter the radius of circle : ");
				int radius=sc.nextInt();
				double k=3.14*radius*radius;;
				System.out.println("Area of Circle is : "+String.format("%.2f", k));
				break;
		case 4:
				System.out.print("Enter the base and height of the traiangle : ");
				int base=sc.nextInt();
				int height=sc.nextInt();
				double h=0.5*base*height;
				System.out.println("Area of Triangle is : "+String.format("%.2f",h));
				break;
		default:
				System.out.println("Enter a valid number!!!!");
				break;
			
		}
		
		

	}

}
