package inheritance;

public class TestEmployee {

	public static void main(String[] args) {
		Employee emp=new Employee("Kalyan",650000,2022,"NINS32456");
		System.out.println("Name of Employee : "+emp.getName());
		System.out.println("Started year     : "+emp.getStartedyear());
		System.out.println("Annual Salary    : "+emp.getSalary());
		System.out.println("Insurance number : "+emp.getInsurancenum());

	}

}
