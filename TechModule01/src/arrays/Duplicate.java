/* program to remove duplicate elements*/
package arrays;
import java.util.*;
public class Duplicate {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size of an array: ");
		int n=sc.nextInt();
		System.out.print("Enter your array of elements: ");
		int[] arr=new int[n];
		int[] t=new int[n];
		int temp;
		/*taking array elements*/
		for(int i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
		/* sorting the array*/
		for(int i=0;i<n;i++) {
			for(int j=i+1;j<n;j++) {
				if(arr[j]<arr[i]) {
					temp=arr[j];
					arr[j]=arr[i];
					arr[i]=temp;
				}
			}
		}
		
		int j=0;
		for(int i=0;i<n-1;i++) {
			if(arr[i]!=arr[i+1]) {
				t[j++]=arr[i];
			}
		}
		t[j++]=arr[n-1];
		for(int i=0;i<j;i++) {
			System.out.print(t[i]+" " );
		}

	}

}
