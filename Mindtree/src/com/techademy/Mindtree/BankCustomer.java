package com.techademy.Mindtree;

public class BankCustomer {
	private int custId=0;
	private String customername=null;
	private String customerAddress=null;
	private String accountType=null;
	private double CustomerBalance=0.0;
	public BankCustomer(int custId,String customername) {
	setCustId(custId);
	setcustomername(customername);
	}
	public BankCustomer(int custId,String customername,String customerAddress,String accountType,double CustomerBalance) {
	setCustId(custId);
	setcustomername(customername);
	setaccountType(accountType);
	setcustomerAddress(customerAddress);
	setCustomerBalance(CustomerBalance);
	}
	public BankCustomer(int custId,String customername,String customerAddress) {
		setCustId(custId);
		setcustomername(customername);
		setcustomerAddress(customerAddress);
		}
		public int getCustId() {
		return custId;
		}
		public void setCustId(int custId) {
		this.custId = custId;
		}
		public String getcustomername() {
		return customername;
		}
		public void setcustomername(String customername) {
		this.customername = customername;
		}
		public String getcustomerAddress() {
		return customerAddress;
		}
		public void setcustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
		}
		public String getaccountType() {
			return accountType;
		}
		public void setaccountType(String accountType) {
			this.accountType = accountType;
		}
		public double getCustomerBalance() {
			return CustomerBalance;
		}
			
		public void setCustomerBalance(double CustomerBalance) {
			this.CustomerBalance = CustomerBalance;
		}

}
