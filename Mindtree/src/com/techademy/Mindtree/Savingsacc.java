package com.techademy.Mindtree;
import java.util.Scanner;

public class Savingsacc {
	
		public static void main(String[] args) {
			
			Scanner sc = new Scanner(System.in);
			SavingsAccount kalyan=new SavingsAccount(25000.0,2,6300422);
			System.out.println("Account Balance : "+kalyan.balance);
			System.out.print("Enter the amount to withdraw : ");
			double c=sc.nextDouble();
			kalyan.withDraw(c);
			System.out.println("Account Balance : "+kalyan.balance);
			System.out.print("Enter interest rate : ");
			int x = sc.nextInt();
			kalyan.calculateInterest(x);
		}

	

}
