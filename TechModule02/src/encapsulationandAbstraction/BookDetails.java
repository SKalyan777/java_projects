package encapsulationandAbstraction;


 class Author {
	private String email,name;
	 char Gender;
	 Author(String n,String e,char G){
		this.name=n;
		this.email=e;
		this.Gender=G;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public char getGender() {
		return Gender;
	}
	public String toString() {
		return name+" \nEmail\t  : "+email+" "+"\nGender\t  : "+Gender;
	}
}
class Book{
	private Author author;
	private String name;
	double price;
	int stockquantity;
	Book(Author a,String n,double p,int stq){
		this.author=a;
		this.name=n;
		this.price=p;
		this.stockquantity=stq;
		
	}
	public String getname() {
		return name;
	}
	public Author getAuthor() {
		return author;
	}
	public double price() {
		return price;
	}
	public int getQuantity() {
		return stockquantity;
	}
	public void setPrice(double price) {
		this.price=price;
	}
	public void setQuantity(int quantity) {
		this.stockquantity=quantity;
	}
	public String toString() {
		return "Book Name : "+name+" \nAuthor    : "+author+" "+"\nPrice\t  : "+price+" "+"\nqtyInStock: "+stockquantity;
	}
}
public class BookDetails{
	
	public static void main(String[] args) {
		Author person=new Author("Kalyan Salluri","sallurikalyan777@gmail.com",'M');
		Book bookdetail=new Book(person,"Art of Programming",999,200);
		System.out.println(bookdetail.toString());

	}

}
