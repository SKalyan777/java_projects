package com.techademy.Mindtree;
import java.util.Scanner;

public class BookDemo {

	public static void main(String[] args) {
		
		BookStore[] books = new BookStore[3];
		books[0] = new BookStore("Daniel Defoe","Robinson Crusoe",15.50,1719);
		books[1] = new BookStore("Joseph Conrad","Heart of Darkness",12.80,1902);
		books[2] = new BookStore("Pat Conroy","Beach Music",9.50,1996);

	}

}
