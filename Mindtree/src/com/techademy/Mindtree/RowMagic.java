package com.techademy.Mindtree;
import java.util.Scanner;

public class RowMagic {
	public static boolean isRowMagic(int[][] a) {
		boolean c=true;
		int sum=0;
		for(int i=0;i<1;i++) {
			for(int j=0;j<a[0].length;j++) {
				sum=sum+a[i][j];
			}	
		}
		for(int i=0;i<a.length;i++) {
			int k=0;
			for(int j=0;j<a[0].length;j++) {
				k=k+a[i][j];
			}
		if(sum!=k) {
			c=false;
			break;
		}
		}
		return c;
		
	}

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		System.out.println("Enter the size of first matrix:");
		int m=a.nextInt();
		int n=a.nextInt();
		int[][] arr1=new int[m][n];
		System.out.println("Enter the matrix elements:");
		for(int i=0;i<m;i++) {
			for(int j=0;j<n;j++) {
				arr1[i][j]=a.nextInt();
			}
		}
		boolean s=isRowMagic(arr1);
		if(s==true) System.out.println("The given array is row magic");
		else {
			System.out.println("The given array won't forms a row magic ");
		}

	}

}
