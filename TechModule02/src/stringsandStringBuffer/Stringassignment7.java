package stringsandStringBuffer;

import java.util.Scanner;

public class Stringassignment7 {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.print("Enter your String : ");
		String str=s.nextLine();
		int n=str.length();
		if(str.charAt(0)=='x'&&str.charAt(n-1)!='x') {
			System.out.println(str.substring(1,n));
			}
		else if(str.charAt(0)=='x'&&str.charAt(n-1)=='x') {
			System.out.println(str.substring(1,n-1));
			}
		else if(str.charAt(0)!='x'&&str.charAt(n-1)=='x') {
			System.out.println(str.substring(0,n-1));
			}
		else {
			System.out.println(str);
		}

	}

}
