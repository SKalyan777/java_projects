package techademy.mindtree2;

public class Customer {
	private String Name;
	private double MobileNo;
	private double feedbackRating;
	Customer(String a,double b,double c){
		this.Name=a;
		this.MobileNo=b;
		this.feedbackRating=c;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getMobileNo() {
		return MobileNo;
	}
	public void setMobileNo(int mobileNo) {
		MobileNo = mobileNo;
	}
	public double getFeedbackRating() {
		return feedbackRating;
	}
	public void setFeedbackRating(double feedbackRating) {
		this.feedbackRating = feedbackRating;
	}
	public String toString(){
	
		return(Name+" : "+feedbackRating+" out of 5");
	}
	

}
