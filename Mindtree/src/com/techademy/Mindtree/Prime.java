package com.techademy.Mindtree;
import java.util.Scanner;

public class Prime {
	static boolean isprime(int n) {
		boolean c=true;
		for(int i=2;i<n;i++) {
			if(n%i==0) {
				c=false;
				break;
			}
		}
		return c;
	}
	public static void main(String[] args) {
		Scanner a =new Scanner(System.in);
		System.out.print("Enter your number : ");
		int n=a.nextInt();
		System.out.println(isprime(n));
}
}
