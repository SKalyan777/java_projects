package inheritance;

public class Employee extends Person {
	public double salary;
	public int StartedYear;
	public String NInsurancenum;
	 Employee(String Name,double s,int k,String n) {
		 super(Name);
		this.salary=s;
		this.StartedYear=k;
		this.NInsurancenum=n;
	}
	public double getSalary() {
		return salary;
	}
	public int getStartedyear() {
		return StartedYear;
	}
	public String getInsurancenum() {
		return NInsurancenum;
	}
}