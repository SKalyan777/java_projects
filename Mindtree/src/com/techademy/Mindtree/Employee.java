package com.techademy.Mindtree;

public class Employee {

	private int empId;
	private String empName;
	private String empDesig;
	private String empDept;
	String[] designations = {"developer","tester","lead","Manager"};
	String[] departments = {"TTH","RCM","Digital","DevOps"};
	Employee(){
		// No Argument Constructor
	}
	Employee(int id,String name,String Designation,String Department){
		empId=id;
		empName=name;
		empDesig=Designation;
		empDept=Department;
		
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpDesig() {
		return empDesig;
	}
	public void setEmpDesig(String empDesig) {
		int c=0;
		for(String s:designations) {
			if(s.equalsIgnoreCase(empDesig)) {
				c=1;
				break;
			}
		}
		if(c==1) {
		this.empDesig = empDesig;}
		else {
			System.out.println("Enter a valid designation!!!!!");
		}
	}
	public String getEmpDept() {
		return empDept;
	}
	public void setEmpDept(String empDept) {
		int d=0;
		for(String s:departments) {
			if(s.equalsIgnoreCase(empDept)) {
				d=1;
				break;
			}
		}
		if(d!=0) {
		this.empDept = empDept;}
		else {
			System.out.println("Enter a valid department name !!!!");
		}
	}
	
}
