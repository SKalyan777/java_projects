package com.techademy.Mindtree;
import java.util.Scanner;

public class TransposeofMatrix {

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		System.out.println("Enter the size of matrix rows X Columns");
		int r=a.nextInt();
		int c=a.nextInt();
		int[][] arr=new int[r][c];
		System.out.println("Enter the elements of matrix");
		for(int i=0;i<r;i++) {
			for(int j=0;j<c;j++) {
				arr[i][j]=a.nextInt();
			}
		}
		int[][] T = new int[c][r];
        for(int i=0;i<r;i++) {
        	for(int j=0;j<c;j++) {
        		T[j][i]=arr[i][j];
        	}
        }
        System.out.println("Transpose of the matrix");
		for(int i=0;i<c;i++) {
			for(int j=0;j<r;j++) {
				System.out.print(T[i][j]+" ");
			}
			System.out.println();
		}
        

		
		

	}

}
