/*Program to find whether the last digit of two numbers are equal or not*/
package controlStstements;
import java.util.Scanner;
public class Lastdigit {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter two numbers:");
		int a=input.nextInt();
		int b=input.nextInt();
		if(a%10==b%10) {
			System.out.println("True");
		}
		else {
			System.out.println("False");
		}

	}

}
