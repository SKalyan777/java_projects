package com.techademy.Mindtree;

import java.util.Scanner;
public class Factorial {

	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		long c=1;
		for(int i=2;i<=n;i++) {
			c=c*i;
		}
		System.out.print("The factorial of "+n+" is "+c);
	}

}
