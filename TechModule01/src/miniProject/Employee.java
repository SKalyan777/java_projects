package miniProject;
import java.util.Scanner;
import java.lang.*;
public class Employee {
	
	/* Transforming the data which is in table form  into the array form*/
	
	int[] Employee_id = { 1001, 1002, 1003, 1004, 1005, 1006, 1007 };
     String[] Emp_name = { "Ashish", "Sushma", "Rahul", "Chahat", "Ranjan", "Suman", "Tanmay" };
    String[] Join_date = { "01/04/2009", "23/08/2012", "12/11/2008", "29/01/2013", "16/07/2005", "1/1/2000",
            "12/06/2006" };
     char[] Designation_code = { 'e', 'c', 'k', 'r', 'm', 'e', 'c' };
     String[] Department = { "R&D", "PM", "Acct", "Front Desk", "Engg", "Manufacturing", "PM" };
     int[] Basic = { 20000, 30000, 10000, 12000, 50000, 23000, 29000 };
     int[] HRA = { 8000, 12000, 8000, 6000, 20000, 9000, 12000 };
     int[] IT = { 3000, 9000, 1000, 2000, 20000, 4400, 10000 };
     String Designation[]= {"Engineer","Consultant","Clerk","Receptionist","Manager"};
     
     /* Assigning DA to their respective designation by using designation code*/
     
     public int DA(int i){
    	 char c=Designation_code[i];
    	 switch(c) {
    	 case 'e': return 20000;
         case 'c': return 32000;
         case 'k': return 12000;
         case 'r': return 15000;
         case 'm': return 40000;
    	 }
    	 return 0;
     }
     /* Calculation of employee salaries from Basic,HRA,DA,IT*/
     
     public int salary(int i) {
    	 if(i==-1) return -1;
    	 else {
    		 return(Basic[i]+HRA[i]+DA(i)-IT[i]);
    	 }
     }
     public String Designationname(int i) {
         char f = Designation_code[i];
         switch(f) {
             case 'e': return "Engineer";
             case 'c': return "Consultant";
             case 'k': return "Clerk";
             case 'r': return "Receptionist";
             case 'm': return "Manager";
         }
         return null;
     }
     public int employee_index(int k) {
    	 int c=-1;
    	 for(int i=0;i<Employee_id.length;i++) {
    		 if(Employee_id[i]==k) {
    			 c=i;
    			 break;}
    		 }
    		 return c;
    	 }
	public static void main(String[] args){
		int emp_id=Integer.parseInt(args[0]);
		Employee emp=new Employee();
		if(emp.employee_index(emp_id)!=-1) {
			int m=emp.employee_index(emp_id);
			System.out.println("Emp No."+"    "+"Emp Name"+"    "+"Department"+"    "+"Designation"+"    "+"Salary");
			System.out.printf("%4d       ", emp_id);
			System.out.printf("%6s ", emp.Emp_name[m]);
			System.out.printf("%10s       ",emp.Department[m]);
			System.out.printf("%12s      ", emp.Designationname(m));
			System.out.printf("%5d\n", emp.salary(m));		
			
		}
		
		else {
			System.out.println("There is no employee with empid :"+emp_id);
		}

}
}
