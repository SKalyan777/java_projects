package classesandObjects;

public class Box {
	double depth;
	double width;
	double height;
	Box(int w,int h,int d){
		
		this.width=w;
		this.depth=d;
		this.height=h;
	
	}
	public double Volume() {
		return width*depth*height;
	}

	public static void main(String[] args) {
		Box c =new Box(12,13,14);
		System.out.println("The volume is "+c.Volume());
		

	}

}
