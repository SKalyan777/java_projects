package com.greatlearning;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Books")
public class Books {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="Book Name")
	private String BookName;
	@Column(name="Aythor Name")
	private String AuthorName;
	@Column(name="Description")
	private String Description;
	@Column(name="Genre")
	private String Genre;
	@Column(name="price")
	private float price;
	
	public Books() {
		
	}
	public Books(String bookName, String authorName, String description, String genre, float price) {
		super();
		BookName = bookName;
		AuthorName = authorName;
		Description = description;
		Genre = genre;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return BookName;
	}
	public void setBookName(String bookName) {
		BookName = bookName;
	}
	public String getAuthorName() {
		return AuthorName;
	}
	public void setAuthorName(String authorName) {
		AuthorName = authorName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getGenre() {
		return Genre;
	}
	public void setGenre(String genre) {
		Genre = genre;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Books [id=" + id + ", BookName=" + BookName + ", AuthorName=" + AuthorName + ", Description="
				+ Description + ", Genre=" + Genre + ", price=" + price + "]";
	}

}
