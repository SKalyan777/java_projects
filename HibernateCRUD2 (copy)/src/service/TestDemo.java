package service;
import entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestDemo {

	public static void main(String[] args) {
		System.out.println("connecting to database");
		SessionFactory factory = null;
		Session session = null;
	
		 factory = new Configuration()
					.configure("hibernate.cfg.xml").addAnnotatedClass(Teacher.class)
					.buildSessionFactory();
			System.out.println("Created");
			Teacher t1 = new Teacher("kalyan","sallur","salluri@gmail.com");
			Teacher t2 = new Teacher("sai","vebkjat","omgole@gmail.com");
			Teacher t3 = new Teacher("rgukt","rkvalley","nuzivid@gmail.com");
			Teacher teacher = new Teacher("iiit","ongole","rkvalley@gmail.com");
			Transaction tx = null;
			try {
				session = factory.getCurrentSession();
				 tx = session.beginTransaction();
				 int teacherId=1;
				 session.save(teacher);
				 System.out.println("Getting student with id"+ teacherId);
					Teacher tempTeacher = session.get(Teacher.class, teacherId);
					System.out.println(tempTeacher);
				tx.commit();
			}catch(Exception e) {
				System.out.println("error");
				e.printStackTrace();
				tx.rollback();
			}finally {
				session.close();
			}
			
		}

	}


