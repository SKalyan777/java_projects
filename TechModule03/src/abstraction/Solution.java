package abstraction;
import java.util.Scanner;
class Hotel{
	private int hotelid;
	private String hotelName;
	private String dateOfBooking;
	private int noOfRoomsBooked;
	private String wifiFacility;
	private double totalBill;
	Hotel(int hotelid,String hotelName,String dateOfBooking,int noOfRoomsBooked,String wifiFacility,double totalBill){
		this.hotelid=hotelid;
		this.hotelName=hotelName;
		this.dateOfBooking=dateOfBooking;
		this.noOfRoomsBooked=noOfRoomsBooked;
		this.wifiFacility=wifiFacility;
		this.totalBill=totalBill;
	}
	public void setRoomname(int i) {
		this.hotelid=i;
	}
	public void setname(String n) {
		this.hotelName=n;
	}
	public void setbooking(String date) {
		this.dateOfBooking=date;
	}
	public void setroomsBooked(int i) {
		this.noOfRoomsBooked=i;
	}
	public void wifiFacility(String k) {
		this.wifiFacility=k;
	}
	public void setbill(double j) {
		this.totalBill=j;
	}
	public int gethotelid() {
		return hotelid;
	}
	public String gethotelName() {
		return hotelName;
	}
	public String getdateofbooking() {
		return dateOfBooking;
	}
	public String getWifistatus() {
		return wifiFacility;
	}
	public int getrooms() {
		return noOfRoomsBooked;
	}
	public double getBill() {
		return totalBill;
	}
}




public class Solution {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		Hotel[] a=new Hotel[4];
		for(int i=0;i<4;i++) {
			int hotelid=sc.nextInt();sc.nextLine();
			String name=sc.nextLine();
			String booking=sc.nextLine();
			int numrooms=sc.nextInt();sc.nextLine();
			String wifi=sc.nextLine();
			double bill=sc.nextDouble();
			sc.nextLine();
			a[i]=new Hotel(hotelid,name,booking,numrooms,wifi,bill);
					
		}
		
		
	}
		

}
