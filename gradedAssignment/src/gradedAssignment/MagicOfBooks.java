package gradedAssignment;

import java.util.Scanner;

public class MagicOfBooks {
	static Scanner sc = new Scanner(System.in);
	static String conti;
	static String iuser;
	static String searchingbook;
	static String outputuser;
	static User user = new User();
	static book Books = new book();
	static Thread th = new Thread(new MyThread());

	public static void main(String[] args) {
		try {
			th.start();
			System.out.println("Do you want to continue?");
			conti = sc.nextLine();
			while (!conti.toLowerCase().equals("yes") && !conti.toLowerCase().equals("no")) {
				System.out.println("Please enter your answer with these 2 values : yes OR no");
				System.out.println("Do you want to continue?");
				conti = sc.nextLine();
			}

			while (conti.toLowerCase().equals("yes")) {
				init();
				System.out.println("Do you want to continue?");
				conti = sc.nextLine();
			}
		} catch (Exception e) {
			System.out.println("You didn't entered the answer of the question: Do you want to continue?");
			System.exit(1);
		}

	}

	// will check for user name is it allowed or not
	public static boolean checkUser() {
		int count = 0;

		for (String s : user.userName) {

			if (s.toLowerCase().equals(iuser.toLowerCase())) {
			    outputuser=s;
				count++;
			}

		}
		if (count == 0) {
			return false;
		} else {
			return true;
		}

	}

	public static void init() {
		try {
			System.out.println("Please enter your username:");
			iuser = sc.nextLine();
			boolean a = checkUser();
			if (a) {
				System.out.println("Welcome " + outputuser + "!");
				opt();
			} else {
				System.out.println("Sorry! You are not an authorized user");
			}

		} catch (Exception e) {
			System.out.println("You didn't entered the username");
			System.exit(1);
		}

	}
    //to select option
	public static void opt() {
		try {
			System.out.println("**************MENU**************");
			System.out.println("1.Print your books(new,favourite,completed)");
			System.out.println("2.Find book by bookid");
			System.out.println("3.Print the details of a book");
			System.out.println("Please enter your choice");

			int opt = Integer.parseInt(sc.nextLine());
			while (opt > 3 | opt < 0) {
				System.out.println("Please enter your options between 1-3");
				System.out.println("Please enter your choice");
				opt = Integer.parseInt(sc.nextLine());
			}
			switch (opt) {

			case 1:
				info();
				break;
			case 2:
				findbookid();
				break;
			case 3:
				findBook();
				break;

			}
		} catch (Exception e) {
			System.out.println("You didn't entered or entered wrong syntax of option");
			System.exit(1);

		}
	}

	// will find the book id present or not in our collection
	public static void findbookid() {
		try {
			int count = 0;
			System.out.println("Please enter your book id:");
			int bookid = Integer.parseInt(sc.nextLine());
			for (int id : Books.bookId) {
				if (bookid == id) {
					count++;
				}
			}
			if (count == 0) {
				System.out.println("Unavailaible");
			} else {
				System.out.println("Available");
			}
		} catch (Exception e) {
			System.out.println("you didn't entered or entered wrong syntax of the book id");
			System.exit(1);
		}

	}

	// print about users books
	public static void info() {
		user.setdetails(iuser);
		String[] newbook = user.userdetails.get(iuser).get("New Books:").split(",");
		String[] compbook = user.userdetails.get(iuser).get("Completed Books:").split(",");
		String[] favbook = user.userdetails.get(iuser).get("Favourite Books:").split(",");
		System.out.println("New Books:");
		for (String book : newbook)

		{
			System.out.println(book);
		}
		System.out.println("Favourite Books:");
		for (String book : favbook)

		{
			System.out.println(book);
		}
		System.out.println("Completed Books:");
		for (String book : compbook)

		{
			System.out.println(book);
		}

	}

	// will search for book name in our collection
	public static void findBook() {
		try {
			int i = 0;
			int count = 0;
			System.out.println("Enter book name: ");
			searchingbook = sc.nextLine();
			for (String b : Books.bookName) {
				if (b.equalsIgnoreCase(searchingbook)) {
					System.out.println("Author name: " + Books.authorName[i]);
					System.out.println("Description: " + Books.description[i]);
					count++;
				}
				i++;
			}
			if (count == 0) {
				System.out.println("Book not found!");
			}
		} catch (Exception e) {
			System.out.println("You didn't entered the book name");
			System.exit(1);
		}

	}
}