/*biggest number in the given 2-D array*/
package arrays;
import java.util.*;
public class biggestNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int max=0;
		int[][] arr=new int[3][3];
		System.out.println("Enter your 2-D array");
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				arr[i][j]=sc.nextInt();
				if(arr[i][j]>max) {
					max=arr[i][j];
				}
			}
		}
		System.out.println("The biggest number in the given array is "+max);

	}

}
