package com.techademy.Mindtree;

public class Magic2 {

	public static boolean isMagic(int [][] arr){
		int s1=0, s2=0;
		for(int j=0;j<arr.length;j++){
		s1+=arr[j][j];
		s2+=arr[j][arr.length-j-1];
		}
		for(int y=0;y<arr.length;y++){
		int r=0,c=0;
		for(int h=0;h<arr[y].length;h++){
		r+=arr[y][h];
		c+=arr[h][y];
		}
		if(r!=c || c!=s1 ||s1!=s2){
		return false;
		}
		}
		return true;
	}
	public static void main(String[] args) {
		int[][] matrix= {{2,7,6},{9,5,1},{4,3,8}};
		boolean output=isMagic(matrix);
		System.out.println(output);

	}

}
