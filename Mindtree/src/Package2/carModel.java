package Package2;

import java.util.*;
public class carModel {
	private String LicenseNumber;
	private String Model;
	private  double CurrentMileage;
	private int EngineSize;
	
   public String getLicenseNumber() {
		return LicenseNumber;
	}
	public void setLicenseNumber(String licenseNumber) {
		LicenseNumber = licenseNumber;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public double getCurrentMileage() {
		return CurrentMileage;
	}
	public void setCurrentMileage(double currentMileage) {
		CurrentMileage = currentMileage;
	}
	public int getEngineSize() {
		return EngineSize;
	}
	public void setEngineSize(int engineSize) {
		EngineSize = engineSize;
	}
public static void main(String[] args) {
      Scanner sc=new Scanner(System.in);
      System.out.println("Enter the number of cars: ");
      int n=sc.nextInt();
      carModel[]  c=new carModel[n];
      for(int i=0;i<n;i++) {
        c[i]=new carModel();
           }
      for(int i=0;i<n;i++) {
    	  System.out.println("Enter license number of car "+(i+1)+": ");
    	  c[i].setLicenseNumber(sc.next());
    	  System.out.println("Enter model of car "+(i+1)+": ");
    	  sc.nextLine();
    	  c[i].setModel(sc.nextLine());
    	  System.out.println("Enter current mileage of car "+(i+1)+": ");
    	  c[i].setCurrentMileage(sc.nextDouble());
    	  System.out.println("Enter engine size of car "+(i+1)+": ");
    	  c[i].setEngineSize(sc.nextInt());
      	}
      System.out.println("License Number | Model | Current Mileage | Engine Size");
      for(int i=0;i<n;i++) {
    	  System.out.println(c[i].getLicenseNumber()+" "+c[i].getModel()+" "+c[i].getCurrentMileage()+" "+c[i].getEngineSize()+" ltrs");
      }
   }
}
