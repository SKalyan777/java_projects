package com.techademy.Mindtree;
import java.util.Scanner;

public class SumOfArray {

	public static void main(String[] args) {
		Scanner a=new Scanner(System.in);
		System.out.print("Enter the size of array: ");
		int n=a.nextInt();
		int[] arr=new int[n];
		int s=0;
		System.out.println("Enter your elements:");
		for(int i=0;i<n;i++) {
			arr[i]=a.nextInt();
			s+=arr[i];
		}
		System.out.println("The sum of elements of array is :"+s);	

	}

}
