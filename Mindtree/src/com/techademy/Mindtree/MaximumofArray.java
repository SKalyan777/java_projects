package com.techademy.Mindtree;

import java.util.Scanner;
public class MaximumofArray {
	
	static int getMax(int a,int b,int c) {
		int max=(a>b)?(a>c?a:c):(b>c?b:c);
		return max;
	}
	public static void main(String[] args) {
		int maximum=0;
		Scanner a=new Scanner(System.in);
		System.out.print("Enter the first element: ");
		int x=a.nextInt();
		System.out.print("Enter the second element: ");
		int y=a.nextInt();
		System.out.print("Enter the third element: ");
		int z=a.nextInt();
		maximum=getMax(x,y,z);
		System.out.println("Maximum of three numbers is :"+maximum);
	}	
}
